// Trevor Lecrone	lecro007 	4968137
// Al Swenson 		swens893 	4962291
// Zixiang Ma 		maxxx580 	4644999
  #include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "util.h"

void show_error_message(char * lpszFileName)
{
	fprintf(stderr, "Usage: %s [options] [target] : only single target is allowed.\n", lpszFileName);
	fprintf(stderr, "-f FILE\t\tRead FILE as a makefile.\n");
	fprintf(stderr, "-h\t\tPrint this message and exit.\n");
	fprintf(stderr, "-n\t\tDon't actually execute commands, just print them.\n");
	fprintf(stderr, "-B\t\tDon't check files timestamps.\n");
	exit(0);
}

int main(int argc, char **argv)
{
	target_t targets[MAX_NODES]; //List of all the targets. Check structure target_t in util.h to understand what each target will contain.
	int nTargetCount = 0;

	// Declarations for getopt
	extern int optind;
	extern char * optarg;
	int ch;
	// char * format = "f:hnB:";
	char * format = "f:hnB";

	// Variables you'll want to use
	char szMakefile[64] = "Makefile";
	char szTarget[64] = "";
	int i=0;

	// Declarations for optional arguement
	int bFlag = 0;
	int nFlag = 0;


	//init Targets
	for(i=0;i<MAX_NODES;i++)
	{
		targets[i].pid=0 ;
		targets[i].nDependencyCount = 0;
		strcpy(targets[i].szTarget, "");
		strcpy(targets[i].szCommand, "");
		targets[i].nStatus = READY;
	}

	while((ch = getopt(argc, argv, format)) != -1)
	{
		switch(ch)
		{
			case 'f':
				strcpy(szMakefile, strdup(optarg));
				break;
			case 'n':
				//Set flag which can be used later to handle this case.
				nFlag = 1;
				break;
			case 'B':
				//Set flag which can be used later to handle this case.
				bFlag = 1;
				break;
			case 'h':
				show_error_message("make4061");
				return EXIT_SUCCESS;
			default:
				show_error_message(argv[0]);
				exit(1);
		}
	}

	argc -= optind;
	argv += optind;

	if(argc > 1)
	{
		show_error_message(argv[0]);
		return EXIT_FAILURE;
	}

	/* Parse graph file or die */
	if((nTargetCount = parse(szMakefile, targets)) == -1)
	{
		return EXIT_FAILURE;
	}
	//Setting Targetname
	//if target is not set, set it to default (first target from makefile)
	if(argc == 1)
	{
		strcpy(szTarget, argv[0]);
	}
	else
	{
		strcpy(szTarget, targets[0].szTarget);
	}

	// Check if specified target is invalid
	// Exit if specified target is not in Makefile
	if (find_target(szTarget, targets, nTargetCount) == -1){
		printf("make4061: ***No rule to make %s. Stop! \n", szTarget);
		exit(0);
	}

	//show_targets(targets, nTargetCount);

	//Now, the file has been parsed and the targets have been named.
	//You'll now want to check all dependencies (whether they are available targets or files)
	//and then execute the target that was specified on the command line, along with their dependencies, etc.
	//Else if no target is mentioned then build the first target found in Makefile.
	/*
	  INSERT YOUR CODE HERE
	*/

	// check for deplication in makefile
	if (check_duplicate(targets, nTargetCount) == -1){
		exit(0);
	}
	// check if invalid or missing dependencies
	if (check_missing_dependency(targets, nTargetCount) == -1){
		exit(0);
	}

	// pre-process timestamps
	// skip when B flag is set
	int rebuild = 1;
	if (bFlag == 0){
		rebuild = prepare_dependancies(targets, nTargetCount, szTarget);
	}
	// execute dependencies or print commands
	build_target(targets, szTarget, nTargetCount, nFlag);

	return EXIT_SUCCESS;
}
