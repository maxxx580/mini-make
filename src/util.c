/************************
 * util.c
 *
 * utility functions
 *
 ************************/

#include "util.h"
#include <sys/wait.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/***************
 * These functions are just some handy file functions.
 * We have not yet covered opening and reading from files in C,
 * so we're saving you the pain of dealing with it, for now.
 *******/
FILE* file_open(char* filename) {
	FILE* fp = fopen(filename, "r");
	if(fp == NULL) {
		fprintf(stderr, "make4061: %s: No such file or directory.\n", filename);
		exit(1);
	}
	return fp;
}

char* file_getline(char* buffer, FILE* fp) {
	buffer = fgets(buffer, 1024, fp);
	return buffer;
}

//Return -1 if file does not exist
//return 0 if file exists
int is_file_exist(char * lpszFileName)
{
	return access(lpszFileName, F_OK);
}

int get_file_modification_time(char * lpszFileName)
{
	if(is_file_exist(lpszFileName) != -1)
	{
		struct stat buf;
		int nStat = stat(lpszFileName, &buf);
		return buf.st_mtime;
	}

	return -1;
}

// Compares the timestamp of two files
// return 1 if fileName1 newer than fileName2
// return 2 otherwise;
int compare_modification_time(char * fileName1, char * fileName2)
{
	int nTime1 = get_file_modification_time(fileName1);
	int nTime2 = get_file_modification_time(fileName2);

//	printf("%s - %d  :  %s - %d\n", lpsz1, nTime1, lpsz2, nTime2);

	if(nTime1 == -1 || nTime2 == -1)
	{
		return -1;
	}

	if(nTime1 == nTime2)
	{
		return 0;
	}
	else if(nTime1 > nTime2)
	{
		return 1;
	}
	else
	{
		return 2;
	}
}

// makeargv
/* Taken from Unix Systems Programming, Robbins & Robbins, p37 */
int makeargv(const char *s, const char *delimiters, char ***argvp) {
   int error;
   int i;
   int numtokens;
   const char *snew;
   char *t;

   if ((s == NULL) || (delimiters == NULL) || (argvp == NULL)) {
      errno = EINVAL;
      return -1;
   }
   *argvp = NULL;
   snew = s + strspn(s, delimiters);
   if ((t = malloc(strlen(snew) + 1)) == NULL)
      return -1;
   strcpy(t,snew);
   numtokens = 0;
   if (strtok(t, delimiters) != NULL)
      for (numtokens = 1; strtok(NULL, delimiters) != NULL; numtokens++) ;

   if ((*argvp = malloc((numtokens + 1)*sizeof(char *))) == NULL) {
      error = errno;
      free(t);
      errno = error;
      return -1;
   }

   if (numtokens == 0)
      free(t);
   else {
      strcpy(t,snew);
      **argvp = strtok(t,delimiters);
      for (i=1; i<numtokens; i++)
         *((*argvp) +i) = strtok(NULL,delimiters);
   }

   *((*argvp) + numtokens) = NULL;
   return numtokens;
}

void freemakeargv(char **argv) {
   if (argv == NULL)
      return;
   if (*argv != NULL)
      free(*argv);
   free(argv);
}

// Used to find the index of target with targetName = lpszTargetName from the list of targets "t"
int find_target(char * lpszTargetName, target_t * const t, int const nTargetCount)
{
	int i=0;
	for(i=0;i<nTargetCount;i++)
	{
		if(strcmp(lpszTargetName, t[i].szTarget) == 0)
		{
			return i;
		}
	}

	return -1;
}

/* Parsing function to parse the Makefile with name = lpszFileName and return the number of targets.
   "t" will point to the first target. Revise "Arrays and Pointers" section from Recitation 2.
*/
int parse(char * lpszFileName, target_t * const t)
{
	char szLine[1024];
	char * lpszLine = NULL;
	char * lpszTargetName = NULL;
	char * lpszDependency = NULL;
	char **prog_args = NULL;
	FILE * fp = file_open(lpszFileName);
	int nTargetCount = 0;
	int nLine = 0;
	target_t * pTarget = t;
	int nPreviousTarget = 0;

	if(fp == NULL)
	{
		return -1;
	}

	while(file_getline(szLine, fp) != NULL)
	{
		nLine++;
		// this loop will go through the given file, one line at a time
		// this is where you need to do the work of interpreting
		// each line of the file to be able to deal with it later
		lpszLine = strtok(szLine, "\n"); //Remove newline character at end if there is one

		if(lpszLine == NULL || *lpszLine == '#') //skip if blank or comment
		{
			continue;
		}

		//Remove leading whitespace
		while(*lpszLine == ' ')
		{
			lpszLine++;
		}

		//skip if whitespace-only
		if(strlen(lpszLine) <= 0)
		{
			continue;
		}

		//Multi target is not allowed.
		if(*lpszLine == '\t') //Commmand
		{
			lpszLine++;

			if(strlen(pTarget->szTarget) == 0)
			{
				fprintf(stderr, "%s: line:%d *** specifying multiple commands is not allowed.  Stop.\n", lpszFileName, nLine);
				return -1;
			}

			strcpy(pTarget->szCommand, lpszLine);
			if (makeargv(pTarget->szCommand, " ", &prog_args) == -1)
			{
				perror("Error parsing command line");
				exit(EXIT_FAILURE);
			}
			pTarget->prog_args = prog_args;
			nPreviousTarget = 0;
			pTarget++;
		}
		else	//Target
		{
			//check : exist Syntax check
			if(strchr(lpszLine, ':') == NULL)
			{
				fprintf(stderr, "%s: line:%d *** missing separator.  Stop.\n", lpszFileName, nLine);
				return -1;
			}

			//Previous target don't have a command
			if(nPreviousTarget == 1)
			{
				pTarget++;
			}

			//Not currently inside a target, look for a new one
			lpszTargetName = strtok(lpszLine, ":");

			if(lpszTargetName != NULL && strlen(lpszTargetName) > 0)
			{
				strcpy(pTarget->szTarget, lpszTargetName);
				lpszDependency = strtok(NULL, " ");

				while (lpszDependency != NULL)
				{
					strcpy(pTarget->szDependencies[pTarget->nDependencyCount], lpszDependency);
					pTarget->nDependencyCount++;
					lpszDependency = strtok(NULL, " ");
				}

				nTargetCount++;
				nPreviousTarget = 1;
			}
			else //error
			{
				fprintf(stderr, "%s: line:%d *** missing separator.  Stop.\n", lpszFileName, nLine);
				return -1;
			}
		}
	}

	fclose(fp);

	return nTargetCount;
}

// for testing purpose only
void print_char_array_for_test(char *p){
	 printf("test char array: %s\n",p);
}

// Function to print the data structure populated by parse() function.
// Use prog_args as arguments for execvp()
void show_targets(target_t * const t, int const nTargetCount)
{
	int i=0;
	int j=0;
	int k=0;
	for(i=0;i<nTargetCount;i++)
	{
		k = 0;
		// printf("%d. Target: %s  Status: %d\nCommand: %s\nDependency: ", i, t[i].szTarget, t[i].nStatus, t[i].szCommand);

		for(j=0;j<t[i].nDependencyCount;j++)
		{
			printf("%s ", t[i].szDependencies[j]);
		}
		// printf("\nDecomposition of command:\n\t");
		while (t[i].prog_args[k] != NULL) {
			// printf("%d. %s\n\t", k, t[i].prog_args[k]);
			k++;
		}
		// printf("\n\n");
	}
}

// set target status to be FINISHED when target file is newer than its dependencies.
int prepare_dependancies(target_t * targetHead,
				   int const nTargetCount,
				   char targetName[64]){
	// file exits but not one of the targets
	// meaning .c or .h files (bottom level) reached
	if (find_target(targetName, targetHead, nTargetCount) == -1 && is_file_exist(targetName) == 0){
		return 0;
	}
	int index = find_target(targetName, targetHead, nTargetCount);
	target_t * temp = & targetHead[index];
	int i = 0;
	int rebuild = 0;
	int nDepen = (*temp).nDependencyCount;
	// traverse each dependency and check modification time
	// recur on each dependency
	while (i < nDepen){
		// not a target, not in the folder
		// missing .c or .h file
		if (find_target(targetName, targetHead, nTargetCount) == -1
			&& is_file_exist((*temp).szDependencies[i]) == -1){
			printf("invalid dependency: program will exit");
			exit(0);
		}
		//	both file name valid, then check time stamp
		if (compare_modification_time(targetName, (*temp).szDependencies[i]) != 1 &&
			compare_modification_time(targetName, (*temp).szDependencies[i]) != 0){
			rebuild += 1;
		}
		rebuild += prepare_dependancies(targetHead, nTargetCount, (*temp).szDependencies[i]);
		i++;
	}
	if (rebuild == 0 && nDepen!=0){
		(*temp).nStatus = FINISHED;
	}

	return rebuild;
}

// this function takes in the head of target list, specific target, and n flag
// this function traverse dependecy and fork process
void build_target(target_t * targetHead,
				  char targetName[64],
				  int const nTargetCount,
				  int const nFlag){

	int index = find_target(targetName, targetHead, nTargetCount);
	target_t * target = & targetHead[index];
	int i = 0;
	int nDepen = (*target).nDependencyCount;
	(*target).nStatus = 2;
	// printf("we make it here 1 depct= %d\n", nDepen);
	while (i < nDepen){
		int j =find_target((*target).szDependencies[i], targetHead, nTargetCount);
		if (j != -1){
			target_t * temp = & targetHead[j];
			//check if temp is a target
			if ((*temp).nStatus == READY){
				build_target(targetHead, (*target).szDependencies[i], nTargetCount, nFlag);

			}
		}

		i++;
	}

	// print and execute commands
	if(nFlag !=1){
		if (strlen(targetHead[find_target(targetName, targetHead, nTargetCount)].szCommand) != 0){
			printf("%s \n", (char*)targetHead[find_target(targetName, targetHead, nTargetCount)].szCommand);
		}
		int pid = fork();
		if (pid > 0) {
 			int status = 0;
			pid = waitpid(pid, &status, 0);
		}
		else if (pid == 0) {
			int k = sizeof(targetHead[find_target(targetName, targetHead, nTargetCount)].prog_args) + 1;
			char *command[k] ;
			int l = 0 ;
			while(targetHead[find_target(targetName, targetHead, nTargetCount)].prog_args[l] != NULL){
				command[l] = targetHead[find_target(targetName, targetHead, nTargetCount)].prog_args[l];
				l++ ;
			}
			command[l] = NULL;
			//if exec succeded, we canot read anything after it, so we have to change the status here
			(*target).nStatus = FINISHED;
			execvp(command[0], command);
			perror("exec failed");
			exit(-1);

		}
		else{
			perror("fork problem");
			exit(-1);
		}
	}
	// print only and DO NOT execute
	else{
		if (strlen(targetHead[find_target(targetName, targetHead, nTargetCount)].szCommand) != 0){
			printf("%s \n", (char*)targetHead[find_target(targetName, targetHead, nTargetCount)].szCommand);
		}
		(*target).nStatus = FINISHED;
	}
}

int check_duplicate(target_t * targetHead, int const nTargetCount){
	int i;
	// iterate through the list to find if any target is duplicated
	for (i = 0; i < nTargetCount; i++){
		int j;
		for (j = i+1; j < nTargetCount; j++){
			target_t  A = targetHead[i];
			target_t  B = targetHead[j];
			if (strcmp(A.szTarget, B.szTarget) == 0){
				printf("duplication found: program will exit \n");
				return -1;
			}
		}
	}
	return 1;
}

int check_missing_dependency(target_t * targetHead, int const nTargetCount){
	int i;
	// iterate through the list to find if any non-target dependecy is missing
	for (i = 0; i < nTargetCount; i++){
		target_t temp = targetHead[i];
		int j;
		for (j = 0; j < temp.nDependencyCount; j++){
			if (find_target(temp.szDependencies[j], targetHead, nTargetCount) == -1
				&& is_file_exist(temp.szDependencies[j])){
				printf("make4061: ***No rule to make target '%s', needed by ''%s'. Stop\n", (char*)temp.szDependencies[j], (char*)temp.szTarget);
				return -1;
			}
		}
	}
	return 0;
}
