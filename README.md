# CSci4061 F2016 Assignment 1
Login: TO DO   
Sate: 10/01/2016   
Name: Trevor Lecrone, Al Swenson, Zixiang Ma   
Id: 4968137(lecro007), 4962291(swens893), 4644999(maxxx580)   

# Description
make4061 is a simple version of Linux make utility, which is able to compile target files 
based on dependency relation. make4061 is defalted to parse information from "Makefile" and
set target to be "all". make4061 can take in optional commands to ignore timestamp, only 
print commands, change default file and targets.

# How to compile the program
Under directory Project_dist/ and execute the following command:  
$ make     
An executable file make4061 will be produced and move make4061 to testing directory.   

# How to use the program from the shell
$ ./make4061  [-f makefile-name]  [-n] [-B] [-h] [target-name]   
##### Command line options 
-f makefile-name: make4061 parses dependencies from file specified after -f flag and ignores default Makefile.    
-n: make4061 does not execute commands but only prints out command for each target.    
-B: make4061 ignores target timestamps and re-builds all targets.    
-h: make4061 shows all error messages   
target-name: make4061 ignores default target "all" and only rebuilds target specified.   
# Tell us who did what on the program
Al: worked with Trevor on target_build function and debugging issues;   
Trevor: created target_build function and successfully debugged fork-exec-wait.    
Zixiang(Eric): created prepare_dependencies function and created program error-handling  

# Additional instruction
##### Error handling
make4061 is able able to identify fork, exec and wait failure.    
make4061 is able to identify duplicated targets in Makefile    
make4061 is able to identify non-target missing dependencies    
make4061 is able to identify invalid target object     
make4061 is able to identify invalid Makefile     
make4061 is able to identify invalid input flags and arguements     


